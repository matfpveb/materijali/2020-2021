export enum ProductPopularity {
  VeryPopular = 'Very Popular',
  Popular = 'Popular',
  Average = 'Average',
  NotPopular = 'Not popular',
}
