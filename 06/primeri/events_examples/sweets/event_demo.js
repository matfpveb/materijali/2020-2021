const events = require('events');

class SweetEmmiter extends events.EventEmitter {
  constructor(maxNumberOfSweetsPerDay) {
    super();
    this.maxNumberOfSweetsPerDay = maxNumberOfSweetsPerDay;
    this.numberOfServedSweets = 0;
  }
}

const sweetEmitter = new SweetEmmiter(3);

sweetEmitter.on('enoughSweets', (data) => {
  console.log('Oh, oh, it is too much!');
  sweetEmitter.removeListener('newSweet', serveNewSweet);
});

const serveNewSweet = (data) => {
  console.log('New sweet is served: ', data);
  sweetEmitter.numberOfServedSweets += 1;
  if (
    sweetEmitter.numberOfServedSweets == sweetEmitter.maxNumberOfSweetsPerDay
  ) {
    sweetEmitter.emit('enoughSweets');
  }
};

sweetEmitter.on('newSweet', serveNewSweet);

sweetEmitter.emit('newSweet', 'chocolate');
sweetEmitter.emit('newSweet', 'cake');
sweetEmitter.emit('newSweet', 'ice cream');
sweetEmitter.emit('newSweet', 'pudding');

// console.log(sweetEmitter.numberOfServedSweets);
