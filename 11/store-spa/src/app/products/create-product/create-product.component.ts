import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { FormControl, FormGroup, ValidationErrors, Validators } from "@angular/forms";
import { Observable } from "rxjs";
import { Product } from "../models/product";
import { interval, of } from "rxjs";
import { take, skip, switchMap } from "rxjs/operators";
import { ProductState } from "../models/product-state";
import { ProductNameValidator } from "../validators/product-name.validator";
declare const $: any;

type ProgressInfo = { dataValue: number; dataTotal: number; fileName: string; label: string };

interface ICreateProductFormValue {
  name: string;
  price: number;
  description: string;
  forSale: boolean;
  state: ProductState;
  files: string;
}

@Component({
  selector: "app-create-product",
  templateUrl: "./create-product.component.html",
  styleUrls: ["./create-product.component.css"],
})
export class CreateProductComponent implements OnInit {
  ProductStateEnum = ProductState;

  @Output()
  public productCreated: EventEmitter<Product> = new EventEmitter<Product>();

  // Formular za kreiranje novog proizvoda
  createProductForm: FormGroup;
  // Informacije o slikama proizvoda koje se pohranjuju
  selectedFiles: FileList;
  progressInfos: ProgressInfo[] = [];

  constructor() {
    this.initializeCreateProductForm();
  }

  ngOnInit() {
    $(".ui.checkbox").checkbox();
    $(".ui.radio.checkbox").checkbox();
  }

  initializeCreateProductForm(): void {
    this.createProductForm = new FormGroup({
      name: new FormControl("", [Validators.required, Validators.minLength(10), ProductNameValidator]),
      price: new FormControl("", [Validators.required, Validators.min(0.99)]),
      description: new FormControl("", [Validators.required, Validators.minLength(50)]),
      forSale: new FormControl(true, Validators.required),
      state: new FormControl(null, Validators.required),
      files: new FormControl("", Validators.required),
    });
  }

  // Metod koji se poziva kada korisnik odabere datoteke
  selectFiles(event: Event): void {
    this.progressInfos = [];
    this.selectedFiles = (event.target as HTMLInputElement).files;
  }

  // Validacija formulara
  nameHasErrors(): string {
    return this.fieldHasErrors("name") ? "error" : "";
  }

  priceHasErrors(): string {
    return this.fieldHasErrors("price") ? "error" : "";
  }

  descriptionHasErrors(): string {
    return this.fieldHasErrors("description") ? "error" : "";
  }

  forSaleHasErrors(): string {
    return this.fieldHasErrors("forSale") ? "error" : "";
  }

  stateHasErrors(): string {
    return this.fieldHasErrors("state") ? "error" : "";
  }

  filesHasErrors(): string {
    return this.fieldHasErrors("files") ? "error" : "";
  }

  nameErrors(): string[] {
    const errorMessages: string[] = [];
    const errors: ValidationErrors = this.createProductForm.get("name").errors;
    if (!errors) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Product must have a name.");
    }
    if (errors.minlength) {
      errorMessages.push(
        `Product name must have at least ${errors.minlength.requiredLength} characters. ` +
          `You have typed in ${errors.minlength.actualLength} characters.`
      );
    }
    if (errors.productName) {
      errorMessages.push(errors.productName.message);
    }
    return errorMessages;
  }

  private fieldHasErrors(fieldName: string): boolean {
    const errors: ValidationErrors = this.createProductForm.get(fieldName).errors;
    return errors != null;
  }

  // Slanje podataka iz formulara
  async createProduct() {
    // Postoji i polje invalid koje ce biti true ako formular ne prolazi validaciju
    if (this.createProductForm.invalid) {
      window.alert("The form is not valid!");
      return;
    }

    const data = this.createProductForm.value as ICreateProductFormValue;

    await this.createProductOnServer(data);
    this.uploadFiles();

    // Prilikom resetovanja formulara mozemo da navedemo vrednosti samo za neke kontrole,
    // dok ce za sve ostale biti postavljena vrednost null.
    this.createProductForm.reset({ forSale: true });
  }

  // Metod koji treba da kontaktira server i posalje podatke
  private async createProductOnServer(data: any): Promise<boolean> {
    console.log(data);
    // Implementacija na sledecem casu :)
    return true;
  }

  // Metod kojim cemo asinhrono slati sve slike na server
  private uploadFiles(): void {
    for (let i = 0; i < this.selectedFiles.length; i++) {
      this.upload(i, this.selectedFiles.item(i));
    }
  }

  // Metod kojim saljemo jednu sliku na server
  private upload(i: number, file: File): void {
    const progressInfo: ProgressInfo = { dataValue: 0, dataTotal: 0, fileName: file.name, label: "" };
    this.progressInfos[i] = progressInfo;

    // Ovde cemo se praviti da saljemo fajl na server, tako sto cemo napraviti lazni tok
    type UploadProgress = { dataValue: number; dataTotal: number };

    const maxProgress: number = Math.round(Math.random() * 10 + 10);
    const upload: Observable<UploadProgress> = interval(200).pipe(
      take(maxProgress + 1),
      skip(1),
      switchMap((progressNum: number) => {
        // Simuliramo gresku tokom pohranjivanja sa relativno malom verovatnocom
        if (Math.random() < 0.01) {
          throw new Error("Upload error");
        }

        // Pravimo objekat koji ce sadrzati informacije od servera.
        return of({
          dataValue: progressNum,
          dataTotal: maxProgress,
        });
      })
    );

    // Pretplacujemo se na tok da bismo dohvatili informacije o progresu
    upload.subscribe(
      (event: UploadProgress) => {
        progressInfo.dataValue = event.dataValue;
        progressInfo.dataTotal = event.dataTotal;
        const percent: number = this.calculateProgressPercentage(progressInfo);
        progressInfo.label = `${percent}% done`;
        this.progressBar(i, percent);
      },
      (err: Error) => {
        progressInfo.dataValue = 0;
        progressInfo.label = err.message + ": " + file.name;
      }
    );
  }

  private calculateProgressPercentage(progressInfo: ProgressInfo): number {
    return Math.round((progressInfo.dataValue / progressInfo.dataTotal) * 100);
  }

  private progressBar(i: number, percent: number): void {
    const progressBar = $(".ui.progress")[i];
    $(progressBar).progress({ percent });
  }
}
