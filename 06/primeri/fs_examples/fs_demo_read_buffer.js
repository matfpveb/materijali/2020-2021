const fs = require('fs');

const testFilePath = './hello_all.txt';

fs.readFile(testFilePath, (readingError, fileData) => {
  if (readingError) {
    // doslo je do greske prilikom citanja
    // console.log(readingError.code);
    // console.log(readingError.message);
    // console.log(readingError.errno);
    // return;
    throw readingError;
  }

  console.log(fileData);
  console.log(fileData.length);
  console.log(fileData.readInt8());
});
