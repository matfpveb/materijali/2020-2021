import { Component, OnInit } from "@angular/core";
import { Product } from "./products/models/product";
import { ProductState } from "./products/models/product-state";
declare const $: any;

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    // Videti: https://semantic-ui.com/modules/tab.html#/examples
    $(".menu .item").tab();
  }
}
