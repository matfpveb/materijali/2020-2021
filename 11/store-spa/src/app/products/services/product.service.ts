import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Product } from "../models/product";
import { GetProductResponseBody, GetProductsResponseBody } from "./models/get-products-response-body";

@Injectable({
  providedIn: "root",
})
export class ProductService {
  private urls = {
    getProducts: "http://localhost:3000/api/products/",
  };

  constructor(private httpService: HttpClient) {}

  public getProducts(page: number = 1, limit: number = 10): Observable<Product[]> {
    const queryParams: HttpParams = new HttpParams().append("page", page.toString()).append("limit", limit.toString());
    // Saljemo HTTP GET metod serveru i kao odgovor dobijamo objekat paginacije.
    // Transformisemo RxJS tok koji emituje paginaciju u RxJS tok proizvoda,
    // tako sto ulancamo RxJS operator "map".
    const observable: Observable<Product[]> = this.httpService
      .get<GetProductsResponseBody>(this.urls.getProducts, { params: queryParams })
      .pipe(
        // Ovu transformaciju radimo zato sto sa servera dobijamo "obicne" JS objekte.
        // Preciznije receno, server nam vraca objekte zapisane u JSON formatu,
        // pa se oni, nakon parsiranja koje Angular uradi za nas, pretvaraju u "obicne" JS objekte.
        // Medjutim, mi zelimo da radimo sa objektima klase "Product" (koji imaju metode i sl.),
        // pa je zbog toga neophodno raditi transformaciju.
        map((pagination: GetProductsResponseBody) => {
          return pagination.docs.map(
            (doc: GetProductResponseBody) =>
              new Product(
                doc._id,
                doc.name,
                doc.description,
                doc.price,
                doc.forSale,
                doc.state,
                doc.owner,
                doc.imgUrls,
                doc.stars
              )
          );
        })
      );
    return observable;
  }
}
