import { IProduct } from "src/app/products/models/product";
import { IUser } from "src/app/users/models/user";

// Interfejs za POJO objekte o narudzbinama koje dobijamo od servera.
export interface IOrder {
  _id: string;
  // Od servera dobijamo nekada niz identifikatora, a nekada niz proizvoda.
  // U praksi bi trebalo kreirati posebne DTO klase za razlicite povratne vrednosti sa servera.
  // U poljima products i user koristimo uniju radi brze implementacije.
  products: (string | IProduct)[];
  user: string | IUser;
  orderTimestamp: Date;
}

export class Order {
  public constructor(
    public id: string,
    // Samo jedan od nizova productsIds i products ce sadrzati informacije.
    // Drugi ce biti prazan. (Videti OrderService.)
    public productsIds: string[],
    public products: IProduct[],
    // Samo jedno od polja userId i user ce sadrzati informacije.
    // Drugi ce biti prazan (prazna niska ili null). (Videti OrderService.)
    public userId: string,
    public user: IUser,
    public orderTimestamp: Date,
  ) {}
}
