import { Component, OnInit, OnDestroy, AfterViewChecked } from "@angular/core";
import { User } from './users/models/user';
import { Subscription } from 'rxjs';
import { AuthService } from './users/services/auth.service';
declare const $: any;

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit, OnDestroy, AfterViewChecked {
  public user: User;
  private userSub: Subscription;

  public constructor(private authService: AuthService) {
    this.userSub = this.authService.user.subscribe((user: User) => {
      this.user = user;
    });
  }

  public ngOnInit() {
    $(".menu .item").tab();
  }

  public ngOnDestroy(): void {
    this.userSub ? this.userSub.unsubscribe() : null;
  }

  public ngAfterViewChecked(): void {
    // Ponovo inicijalizujemo tab za kreiranje proizvoda ako je dodat u DOM stablo
    $(".menu .item").tab();
  }
}
