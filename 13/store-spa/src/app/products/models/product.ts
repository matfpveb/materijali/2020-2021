import { User } from "src/app/users/models/user";
import { environment } from "src/environments/environment";
import { ProductPopularity } from "./product-popularity";
import { ProductState } from "./product-state";

export interface IProduct {
  _id: string;
  name: string;
  price: number;
  description: string;
  forSale: boolean;
  state: ProductState;
  owner: User;
  imgUrls: string[];
  stars: number;
}

export class Product {
  constructor(
    public id: string,
    public name: string,
    public description: string,
    public price: number,
    public forSale: boolean,
    public state: ProductState,
    public owner: User,
    public imgUrls: string[],
    public stars: number,
  ) {}

  get popularity(): ProductPopularity {
    if (this.stars >= 200) {
      return ProductPopularity.VeryPopular;
    }
    if (this.stars >= 100) {
      return ProductPopularity.Popular;
    }
    if (this.stars >= 50) {
      return ProductPopularity.Average;
    }
    return ProductPopularity.NotPopular;
  }

  get imgSrc(): string {
    return this.getImgSrcByIndex(0);
  }

  public getImgSrcByIndex(index: number): string {
    return environment.fileDownloadUrl + this.imgUrls[index];
  }
}
