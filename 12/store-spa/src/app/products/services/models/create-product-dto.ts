import { ProductState } from '../../models/product-state';

export class CreateProductDTO {
  name: string;
  price: number;
  description: string;
  forSale: boolean;
  state: ProductState;
}
