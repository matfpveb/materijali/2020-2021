import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { User } from "../models/user";

// Ovaj interfejs cemo koristiti za tipiziranost podataka koje je korisnik uneo u formularu
interface IUserFormValue {
  username: string;
  email: string;
  name: string;
}

@Component({
  selector: "app-user-profile",
  templateUrl: "./user-profile.component.html",
  styleUrls: ["./user-profile.component.css"],
})
export class UserProfileComponent implements OnInit {
  user: User = new User("peraperic", "pera@gmail.com", "Pera Peric", "/assets/default-user.png");
  showChangeFields: boolean;

  // Model za formular
  userForm: FormGroup;
  // Informacija o datoteci koji je korisnik odabrao sa diska
  imageToUpload: File;

  constructor() {
    this.showChangeFields = false;
    this.initializeUserFormGroup();
  }

  ngOnInit() {}

  initializeUserFormGroup(): void {
    // Kreiramo objekat klase FormGroup koji ce vezati odgovarajuca polja za unos iz sablona za sebe,
    // kako bi mogao da vrsi validaciju tih polja na reaktivan nacin.
    this.userForm = new FormGroup({
      username: new FormControl(this.user.username, [Validators.required, Validators.pattern(/[a-zA-Z0-9_-]{8,}/)]),
      email: new FormControl(this.user.email, [Validators.required, Validators.email]),
      name: new FormControl(this.user.name, [Validators.required]),
      imgUrl: new FormControl(''),
    });
  }

  enableChangeFields(): void {
    this.showChangeFields = true;
  }

  // File chooser, iako deo formulara, nece sadrzati pune informacije o datoteci, vec samo "laznu putanju",
  // pa zato moramo da zapamtimo detaljne informacije o odabranoj datoteci.
  handleFileInput(event: Event): void {
    const files: FileList = (event.target as HTMLInputElement).files;
    if (!files.length) {
      return;
    }
    this.imageToUpload = files.item(0);
  }

  onSubmitUserForm(): void {
    // Provera da li je formular validan
    if (!this.userForm.valid) {
      window.alert('The form is not valid!');
      return;
    }

    if (!this.imageToUpload) {
      const response: boolean = window.confirm('You haven\'t chosen a new profile picture. Do you want to skip this field?');
      if (!response) {
        return;
      }
    }

    // Vrednost value iz FormGroup cuva POJO objekat sa vrednostima formulara,
    // pa bi bilo lepo da ga tipiziramo - zato smo i napravili interfejs.
    // Ovo nije obavezno raditi, ali jeste dobra praksa.
    const data = this.userForm.value as IUserFormValue;
    console.log(data);

    // Ovde bi se nalazio kod za slanje podataka serveru - to cemo videti sledeceg casa.
    // Server bi trebalo da vrati objekat koji bi sadrzao konkretnu putanju do mesta gde je slika sacuvana.
    // Za sada, samo postavljamo novu vrednost objekta user, pri cemu koristimo podrazumevanu sliku.
    const userData = new User(data.username, data.email, data.name, '/assets/default-user.png');
    this.user = userData;

    // Nad formularima mozemo jednostavno da resetujemo polja za naredni unos pozivom metoda reset().
    // Ako ne prosledimo nista, on ce ocistiti vrednosti polja (tj. postaviti ih na null vrednost).
    // Inace, mozemo proslediti objekat koji ce se koristiti za inicijalizaciju polja
    // (kljucevi u objektu moraju da odgovaraju nazivima polja).
    this.userForm.reset({
      username: this.user.username,
      email: this.user.email,
      name: this.user.name,
      imgUrl: '', // Element <input> koji je tipa "file" moze se programerski postaviti jedino na praznu nisku!
    });
    // "Resetujemo" i sliku
    this.imageToUpload = null;

    this.showChangeFields = false;
  }
}
