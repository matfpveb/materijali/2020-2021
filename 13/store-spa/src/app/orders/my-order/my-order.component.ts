import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { OrderService } from "../services/order.service";
import { Observable } from "rxjs";
import { Order } from "../models/order";
import { switchMap } from "rxjs/operators";
import { IProduct } from "src/app/products/models/product";

@Component({
  selector: "app-my-order",
  templateUrl: "./my-order.component.html",
  styleUrls: ["./my-order.component.css"],
})
export class MyOrderComponent implements OnInit {
  public order: Observable<Order>;

  public constructor(private activatedRoute: ActivatedRoute, private ordersService: OrderService) {
    this.order = this.activatedRoute.paramMap.pipe(
      switchMap((paramMap: ParamMap) => {
        const orderId: string = paramMap.get("orderId");
        return this.ordersService.getOrderById(orderId);
      }),
    );
  }

  ngOnInit() {}

  public getOrderPrice(order: Order): number {
    return order.products
      .map((product: IProduct) => product.price)
      .reduce((left: number, right: number) => left + right, 0);
  }
}
