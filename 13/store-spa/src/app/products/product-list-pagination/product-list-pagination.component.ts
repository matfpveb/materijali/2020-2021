import { Component, OnInit, Input } from "@angular/core";
import { GetProductsResponseBody } from "../services/models/get-products-response-body";

@Component({
  selector: "app-product-list-pagination",
  templateUrl: "./product-list-pagination.component.html",
  styleUrls: ["./product-list-pagination.component.css"],
})
export class ProductListPaginationComponent implements OnInit {
  @Input() pagination: GetProductsResponseBody;

  constructor() {}

  ngOnInit() {}

  public getArrayOfPageNumbers(totalPages: number): number[] {
    return [...Array(totalPages).keys()].map((page: number) => page + 1);
  }
}
