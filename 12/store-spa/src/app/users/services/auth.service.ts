import { Injectable } from "@angular/core";
import { Subject, Observable, of } from "rxjs";
import { User } from "../models/user";
import { JwtService } from "src/app/common/services/jwt.service";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { map, catchError } from "rxjs/operators";
import { IJWTTokenData } from "src/app/common/services/models/jwt-token-data";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  // Subjekat ispod cemo koristiti za postavljanje novih vrednosti za User-a
  private readonly userSubject: Subject<User> = new Subject<User>();
  // Tok ispod cemo koristiti za pretplacivanje komponenti za nove vrednosti User-a.
  // Kao izvor za ovaj tok se koristi subjekat iznad.
  // Kombinacija Subject + Observable na ovaj nacin predstavlja jednu od standardnih
  // Angular praksi za razmenu podataka izmedju komponenti pomocu servisa
  // (najcesce se koristi kada ne postoji "prirodan" tok podataka uz/niz hijerarhiju komponenti).
  // Videti detaljnije ovde: https://angular.io/guide/component-interaction#parent-and-children-communicate-via-a-service
  public readonly user: Observable<User> = this.userSubject.asObservable();

  private readonly urls = {
    registerUser: "http://localhost:3000/api/users/register/",
    loginUser: "http://localhost:3000/api/users/login/"
  };

  constructor(private http: HttpClient, private jwtService: JwtService) {}

  public sendUserDataIfExists(): User {
    // Dohvatamo podatke iz JWT
    const payloadData: IJWTTokenData = this.jwtService.getDataFromToken();
    // Kreiramo objekat za trenutno registrovanog korisnika i prosledjujemo ga svim pretplacenim komponentama
    const user: User = payloadData
      ? new User(payloadData.id, payloadData.username, payloadData.email, payloadData.name, payloadData.imgUrl)
      : null;
    this.userSubject.next(user);
    return user;
  }

  public registerUser(username: string, password: string, name: string, email: string): Observable<User> {
    const body = { username, password, name, email };
    return this.http.post<{ token: string }>(this.urls.registerUser, body).pipe(
      // Operator catchError sluzi za obradu gresaka u HTTP komunikaciji
      catchError((error: HttpErrorResponse) => this.handleError(error)),
      map((response: { token: string }) => this.mapResponseToUser(response))
    );
  }

  public loginUser(username: string, password: string): Observable<User> {
    const body = { username, password };
    return this.http.post<{ token: string }>(this.urls.loginUser, body).pipe(
      // Operator catchError sluzi za obradu gresaka u HTTP komunikaciji
      catchError((error: HttpErrorResponse) => this.handleError(error)),
      map((response: { token: string }) => this.mapResponseToUser(response))
    );
  }

  public logoutUser(): void {
    // Uklanjamo JWT iz memorije i saljemo svim komponentama da korisnik "ne postoji"
    this.jwtService.removeToken();
    this.userSubject.next(null);
  }

  private handleError(error: HttpErrorResponse): Observable<{ token: string }> {
    const serverError: { message: string; status: number; stack: string } = error.error;
    window.alert(`There was an error: ${serverError.message}. Server returned code: ${serverError.status}`);
    return of({ token: this.jwtService.getToken() });
  }

  private mapResponseToUser(response: { token: string }): User {
    // Cuvamo JWT u memoriju veb pregledaca
    this.jwtService.setToken(response.token);
    // Saljemo podatke o korisniku na osnovu postavljenog JWT tokena
    return this.sendUserDataIfExists();
  }
}
