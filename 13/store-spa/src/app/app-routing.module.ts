import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ProductListComponent } from "./products/product-list/product-list.component";
import { LoginFormComponent } from "./users/login-form/login-form.component";
import { RegisterFormComponent } from "./users/register-form/register-form.component";
import { UserProfileComponent } from "./users/user-profile/user-profile.component";
import { LogoutComponent } from "./users/logout/logout.component";
import { UserAuthenticatedGuard } from "./users/guards/user-authenticated.guard";
import { ProductDetailsComponent } from "./products/product-details/product-details.component";
import { ShoppingCartComponent } from "./orders/shopping-cart/shopping-cart.component";
import { MyOrderComponent } from "./orders/my-order/my-order.component";
import { CreateProductComponent } from "./products/create-product/create-product.component";

// Putanje u nizu ispod ne smeju pocinjati karakterom '/'.
const routes: Routes = [
  { path: "", component: ProductListComponent },
  { path: "login", component: LoginFormComponent },
  { path: "register", component: RegisterFormComponent },
  { path: "logout", component: LogoutComponent },
  { path: "user", component: UserProfileComponent, canActivate: [UserAuthenticatedGuard] },
  { path: "create-product", component: CreateProductComponent, canActivate: [UserAuthenticatedGuard] },
  { path: "products/:productId", component: ProductDetailsComponent },
  { path: "orders", component: ShoppingCartComponent, canActivate: [UserAuthenticatedGuard] },
  { path: "my-orders/:orderId", component: MyOrderComponent, canActivate: [UserAuthenticatedGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
