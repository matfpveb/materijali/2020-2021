import { environment } from 'src/environments/environment';

export class User {
  constructor(public id: string, public username: string, public email: string, public name: string, private _imgUrl: string) {}

  // Sliku korisnika sada cuvamo na serveru, a od servera dobijamo samo naziv datoteke,
  // tako da je potrebno da napravimo ocitavac koji ce izracunati celu putanju do slike.
  // S obzirom da je URL do direktorijuma servera isti za sve slike na nivou aplikacije,
  // taj URL mozemo dodati kao deo "okruzenja".
  public get imgUrl(): string {
    return `${environment.fileDownloadUrl}/${this._imgUrl}`;
  }
}
