# 12. sedmica vežbi

## Angular - Autentikacija, JWT, localStorage i pohranjivanje datoteka

- [Aplikacija sa časa](./store-spa/). Izgled aplikacije:

![Store SPA](./app-images/store-spa4-1.png)
![Store SPA](./app-images/store-spa4-2.png)
![Store SPA](./app-images/store-spa4-3.png)
![Store SPA](./app-images/store-spa4-4.png)
![Store SPA](./app-images/store-spa4-5.png)
![Store SPA](./app-images/store-spa4-6.png)
![Store SPA](./app-images/store-spa4-7.png)
![Store SPA](./app-images/store-spa4-8.png)
![Store SPA](./app-images/store-spa4-9.png)
