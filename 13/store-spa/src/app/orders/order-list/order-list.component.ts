import { Component, OnInit } from "@angular/core";
import { OrderService } from "../services/order.service";
import { Observable } from "rxjs";
import { Order } from "../models/order";

@Component({
  selector: "app-order-list",
  templateUrl: "./order-list.component.html",
  styleUrls: ["./order-list.component.css"],
})
export class OrderListComponent implements OnInit {
  public orders: Observable<Order[]>;

  public constructor(private orderService: OrderService) {
    this.orders = this.orderService.getOrders();
  }

  ngOnInit() {}
}
