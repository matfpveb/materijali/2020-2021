import { Component, OnInit, OnDestroy } from "@angular/core";
import { OrderService } from "../services/order.service";
import { Product } from "src/app/products/models/product";
import { Subscription, Observable } from "rxjs";
import { Order } from "../models/order";

@Component({
  selector: "app-shopping-cart",
  templateUrl: "./shopping-cart.component.html",
  styleUrls: ["./shopping-cart.component.css"],
})
export class ShoppingCartComponent {
  public order: Observable<Order>;

  public constructor(private orderService: OrderService) {}

  public get isBasketEmpty(): boolean {
    return this.orderService.isBasketEmpty;
  }

  public get products(): Product[] {
    return this.orderService.productsInBasket;
  }

  public removeProductFromBasket(product: Product) {
    this.orderService.removeProductFromBasket(product);
  }

  public placeOrder(): void {
    this.order = this.orderService.postOrder();
  }
}
