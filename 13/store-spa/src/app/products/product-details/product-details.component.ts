import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { ProductService } from "../services/product.service";
import { Observable } from "rxjs";
import { Product } from "../models/product";
import { switchMap } from "rxjs/operators";
import { ProductPopularity } from "../models/product-popularity";
import { AuthService } from "src/app/users/services/auth.service";
import { OrderService } from "src/app/orders/services/order.service";

@Component({
  selector: "app-product-details",
  templateUrl: "./product-details.component.html",
  styleUrls: ["./product-details.component.css"],
})
export class ProductDetailsComponent implements OnInit {
  public ProductPopularityEnum = ProductPopularity;
  public product: Observable<Product>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private productService: ProductService,
    private authService: AuthService,
    private orderService: OrderService,
  ) {
    this.product = this.activatedRoute.paramMap.pipe(
      switchMap((params: ParamMap) => {
        const productId: string = params.get("productId");
        return this.productService.getProductDetailsById(productId);
      }),
    );

    this.authService.sendUserDataIfExists();
  }

  ngOnInit() {}

  public canBuy(): boolean {
    return this.authService.userLoggedIn;
  }

  public addProductToBasket(p: Product): void {
    const alreadyAdded: boolean = this.orderService.addProductToBasket(p);
    if (alreadyAdded) {
      window.alert("You have already added this product to your shopping cart!");
    } else {
      window.alert("You have successfully added this product to your shopping cart!");
    }
  }
}
