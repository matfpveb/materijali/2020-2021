# 2020-2021

Ovaj repozitorijum sadrži zvanične materijale sa časova vežbi tokom akademske 2020/2021. godine.

## Primer ispita

Informacije o primeru ispita možete pronaći [ovde](./primer-ispita/)

## Rokovi iz prošle godine

Primere ispitnih rokova iz prethodnih godina možete pronaći na narednoj vezi:

- [2019/2020](https://gitlab.com/matfpveb/materijali/2019-2020/-/tree/master/ispiti)
