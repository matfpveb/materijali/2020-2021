let bazaProteina =
  '[{"sekvenca":"MSMDISDFYQTFFDEADELL","naziv":"Chem. protein CheA","duzina":654,"organizam":"EC","sadrzaj uredjenosti":0.0993,"uredjenost":"Disorder","funkcije":["Flexible linker/spacer"]},{"sekvenca":"MTTQVPPSALLPLNPEQLAR","duzina":599,"naziv":"SRF a-c","organizam":"EC","sadrzaj uredjenosti":0.2888,"uredjenost":"Disorder","funkcije":["Molecular recognition effector"]},{"sekvenca":"MKGTARVQTAREMKINGEIR","duzina":180,"organizam":"EC","naziv":"TIF 333","sadrzaj uredjenosti":0.0667,"uredjenost":"Order","funkcije":["Flexible linker/spacer","Nuclear magnetic resonance","Protein binding"]},{"sekvenca":"MGDEDWEAEINPHMSSYVPI","duzina":690,"organizam":"HS","naziv":"Protein 947","sadrzaj uredjenosti":0.2956,"uredjenost":"Disorder","funkcije":["Liquid-liquid phase separation","Protein binding"]},{"sekvenca":"MASSCAVQVKLELGHRAQVR","duzina":768,"organizam":"HS","naziv":"Isoform 2 of PATP-dep. RNA helix. DDX478","sadrzaj uredjenosti":0.139,"uredjenost":"Order","funkcije":["Molecular recognition assembler","Protein binding"]}]';

// RESENJE:

bazaProteina = JSON.parse(bazaProteina);

function f1() {
}

function f2(protein, k) {
}

function f3() {
}

function f4() {
}

// TESTOVI:

const protein_sa_najduzom_sekvencom = f1();
console.log(
  protein_sa_najduzom_sekvencom === 'Isoform 2 of PATP-dep. RNA helix. DDX478'
);

const k_meri = f2(bazaProteina[0], 3);
console.log(k_meri.length === 17); // Provera broja k-mera
console.log(k_meri[0].length === 3); // Provera duzine jednog k-mera
console.log(
  k_meri.join(' ') ===
    'M,S,M S,M,D M,D,I D,I,S I,S,D S,D,F D,F,Y F,Y,Q Y,Q,T Q,T,F T,F,F F,F,D F,D,E D,E,A E,A,D A,D,E D,E,L'
);

const recnik = f3();
console.log(recnik['Flexible linker/spacer'].length === 2);

const najzastupljenija_funkcija = f4();
console.log(najzastupljenija_funkcija === 'Protein binding');
