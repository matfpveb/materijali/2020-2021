import { Component, OnInit } from '@angular/core';
import { Product } from './products/models/product';
declare const $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  products: Product[] = [
    new Product(
      'New Lenovo laptop 2020',
      'A very good laptop. Unpacked!',
      450,
      true,
      'assets/default-product.jpg',
      'Pera Peric',
      10
    ),
    new Product('iPhone XS', 'Used for a year', 550, true, 'assets/default-product.jpg', 'Pera Peric', 50),
    new Product(
      'The Way of Kings Book One [SOLD!]',
      'Fantasy novel by Brandon Sanderson',
      10,
      false,
      'assets/default-product.jpg',
      'Pera Peric',
      250
    ),
  ];

  constructor() {}

  ngOnInit() {
    // Videti: https://semantic-ui.com/modules/tab.html#/examples
    $('.menu .item').tab();
  }

  onProductCreated(product: Product) {
    this.products.push(product);
  }
}
