import { Component, ElementRef, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { Product } from '../models/product';
// Ukljucili smo jQuery biblioteku preko CDN-a,
// pa zbog toga Angular ne zna za nju u fazi kompilacije.
// Zato moramo da deklarisemo da ce biti dinamickog tipa any.
// Alternativno resenje sa instalacijom jQuery preko npm-a:
// https://medium.com/better-programming/how-to-include-and-use-jquery-in-angular-cli-project-592e0fe63176
declare const $: any;

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css'],
})
export class CreateProductComponent implements OnInit {
  @ViewChild('inputName', { static: false })
  private inputName: ElementRef;

  @ViewChild('inputPrice', { static: false })
  private inputPrice: ElementRef;

  @ViewChild('inputDescription', { static: false })
  private inputDescription: ElementRef;

  @ViewChild('inputForSale', { static: false })
  private inputForSale: ElementRef;

  @Output()
  public productCreated: EventEmitter<Product> = new EventEmitter<Product>();

  constructor() {}

  ngOnInit() {
    // Videti: https://semantic-ui.com/collections/form.html#checkbox
    // Primetite da ovaj kod ne bi radio ako bi se nalazio u konstruktoru iznad,
    // zato sto pogled komponente jos uvek nije inicijalizovan.
    // Pogledati skriptu za vise detalja o zivotnom toku komponenti.
    $('.ui.checkbox').checkbox();
  }

  createAProduct() {
    const name: string = (this.inputName.nativeElement as HTMLInputElement).value;
    const price: number = Number.parseFloat((this.inputPrice.nativeElement as HTMLInputElement).value);
    const description: string = (this.inputDescription.nativeElement as HTMLTextAreaElement).value;
    const forSale: boolean = (this.inputForSale.nativeElement as HTMLInputElement).checked;

    if (name === '' || price === NaN || description === '') {
      window.alert('One of the required fields is missing a value!');
      return;
    }

    const newProduct: Product = new Product(
      name,
      description,
      price,
      forSale,
      'assets/default-product.jpg',
      'Pera Peric',
      0
    );
    this.productCreated.emit(newProduct);
  }
}
