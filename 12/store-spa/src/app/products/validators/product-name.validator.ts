import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export const ProductNameValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  if (control.value === null) {
    return { productName: { message: 'Product name must be entered' } };
  }
  const nameParts: string[] = control.value.trim().split(' ').filter((namePart: string) => namePart.trim().length > 0);

  if (nameParts.length === 0) {
    return { productName: { message: 'Product name cannot consist only of whitespaces' } };
  }
  if (nameParts.every((namePart: string) => namePart.match(/^[0-9\s]+$/))) {
    return { productName: { message: 'Product name cannot consist only of numbers' } };
  }

  return null;
};
