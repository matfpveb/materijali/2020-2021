import { IProduct } from '../../models/product';

export interface GetProductsResponseBody {
  docs: IProduct[];
  hasNextPage: boolean;
  hasPrevPage: boolean;
  limit: number;
  nextPage?: number;
  page: number;
  pagingCounter: number;
  prevPage?: number;
  totalDocs: number;
  totalPages: number;
}
