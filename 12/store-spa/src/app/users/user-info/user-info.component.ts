import { Component, OnDestroy } from '@angular/core';
import { User } from '../models/user';
import { AuthService } from '../services/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnDestroy {
  public isLogin: boolean = true;
  public user: User;
  private userSub: Subscription;

  constructor(private authService: AuthService) {
    this.userSub = this.authService.user.subscribe((user: User) => {
      this.user = user;
    });
    this.authService.sendUserDataIfExists();
  }

  ngOnDestroy(): void {
    this.userSub ? this.userSub.unsubscribe() : null;
  }

  public switchBetweenLoginAndRegistration(): void {
    this.isLogin = !this.isLogin;
  }

}
