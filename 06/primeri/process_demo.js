// pid: process id
console.log(process.pid);

// argv: argumenti navedeni prilikom pokretanja
console.log(process.argv);

// env: sistemske promenljive pridruzene procesu
console.log(process.env);

// sistemska promenljiva process.env.NODE_ENV sa cestim vrednostima 'production' i 'development'
if (process.env.NODE_ENV == 'development') {
  console.log('razvoj aplikacije ...');
} else {
  console.log('produkcija...');
}

// dogadjaj uncaughtException: pomoc pri obradi gresaka
// process.on('uncaughtException', function (error) {
//   console.error('Error handling: ', error.message);
// //   console.error(error.stack);
// });
// throw Error('test error');

// dogadjaj exit: pokretanje rutina prilikom zavrsetka skripta
// process.on('exit', function () {
//   console.log('Process is about to end.');
// });
