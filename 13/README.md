# 13. sedmica vežbi

## Angular - Rutiranje, filteri

- [Sekcije 8.9 i 8.10 iz skripte](http://www.nikolaajzenhamer.rs/assets/pdf/pzv.pdf)
- [Node.js server za prodavnicu (dopunjena implementacija u odnosu na prethodnu)](./store-server/)
    - [Informacije o serveru](./store-server/README.md)
- [Početni podaci za popunjavanje BP](./store-data)
    - Nakon preuzimanja koda, potrebno je izvršiti naredbe:
        - `chmod +x import-store.sh`
        - `./import-store.sh`
- [Aplikacija sa časa](./store-spa/). 
