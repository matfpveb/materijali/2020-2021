import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { JwtService } from "src/app/common/services/jwt.service";
import { User } from "../models/user";
import { map, catchError } from "rxjs/operators";
import { AuthService } from "./auth.service";

@Injectable({
  providedIn: "root"
})
export class UserService {
  private readonly urls = {
    patchUser: "http://localhost:3000/api/users/",
    patchUserProfileImage: "http://localhost:3000/api/users/profile-image"
  };

  constructor(private http: HttpClient, private jwtService: JwtService, private authService: AuthService) {}

  public patchUserData(username: string, email: string, name: string): Observable<User> {
    const body = { username, email, name };
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);
    return this.http
      .patch<{ token: string }>(this.urls.patchUser, body, { headers })
      .pipe(
        catchError((error: HttpErrorResponse) => this.handleError(error)),
        map((response: { token: string }) => this.mapResponseToUser(response))
      );
  }

  public patchUserProfileImage(file: File): Observable<User> {
    // Datoteke ne mozemo da saljemo "tek tako",
    // vec ih moramo serijalizovati kao deo FormData.
    const body: FormData = new FormData();
    body.append("file", file);

    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);
    return this.http
      .patch<{ token: string }>(this.urls.patchUserProfileImage, body, { headers })
      .pipe(
        catchError((error: HttpErrorResponse) => this.handleError(error)),
        map((response: { token: string }) => this.mapResponseToUser(response))
      );
  }

  // Domaci zadatak: Primetimo da se metod handleError ispod javlja i u ovom servisu i u AuthService.
  // Refaktorisite kod tako sto ce se ova obrada HTTP gresaka javiti kao deo natklase za ova dva servisa.

  private handleError(error: HttpErrorResponse): Observable<{ token: string }> {
    const serverError: { message: string; status: number; stack: string } = error.error;
    window.alert(`There was an error: ${serverError.message}. Server returned code: ${serverError.status}`);
    return of({ token: this.jwtService.getToken() });
  }

  private mapResponseToUser(response: { token: string }): User {
    // Cuvamo JWT u memoriju veb pregledaca
    this.jwtService.setToken(response.token);
    // Saljemo podatke o korisniku na osnovu postavljenog JWT tokena
    return this.authService.sendUserDataIfExists();
  }
}
