console.log(global);

// promenljive koje su globalno vidljive i koje se mogu koristiti i na nivou drugih modula
global.message = 'Hello there!';

// promenljive koje imaju opseg vazenja tekuceg modula
let message = 'Confusing hello!';

message = 'Hmh...';

console.log(global.message);
console.log(message);
