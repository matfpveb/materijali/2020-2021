# Zanimljivosti

### Orijentacija: 
* Road Map, web developer: https://github.com/kamranahmedse/developer-roadmap

### Styleguides: 
* https://github.com/airbnb/javascript
* https://google.github.io/styleguide/jsguide.html
* https://standardjs.com/

### JavaScript Scope Vizualizacija
* https://tylermcginnis.com/javascript-visualizer/

