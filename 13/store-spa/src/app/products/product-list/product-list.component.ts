import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { Product } from "../models/product";
import { ProductService } from "../services/product.service";
import { GetProductsResponseBody } from "../services/models/get-products-response-body";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { switchMap } from "rxjs/operators";

@Component({
  selector: "app-product-list",
  templateUrl: "./product-list.component.html",
  styleUrls: ["./product-list.component.css"],
})
export class ProductListComponent implements OnInit {
  pagination: Observable<GetProductsResponseBody>;

  constructor(private productService: ProductService, private activatedRoute: ActivatedRoute) {
    this.pagination = this.activatedRoute.queryParamMap.pipe(
      switchMap((queryMap: ParamMap) => {
        const page: string = queryMap.get("page");
        const pageNum: number = Number.parseInt(page) || 1;
        return this.productService.getProductsWithPagination(pageNum, 5);
      }),
    );
  }

  ngOnInit() {}
}
