import { Component, Input, OnInit, OnDestroy } from "@angular/core";
import { Product } from "../models/product";
import { ProductPopularity } from "../models/product-popularity";
import { Observable, Subscription, interval } from "rxjs";

@Component({
  selector: "app-product",
  templateUrl: "./product.component.html",
  styleUrls: ["./product.component.css"]
})
export class ProductComponent implements OnInit, OnDestroy {
  public ProductPopularityEnum = ProductPopularity;

  @Input()
  public product: Product;

  public interval: Observable<number>;
  private intervalSubscription: Subscription;

  public constructor() {}

  public ngOnInit(): void {
    if (this.product.imgUrls.length > 0) {
      this.interval = interval(1000);
      this.intervalSubscription = this.interval.subscribe(() => {
        this.product.nextImage();
      });
    }
  }

  public ngOnDestroy(): void {
    this.intervalSubscription ? this.intervalSubscription.unsubscribe() : null;
  }
}
