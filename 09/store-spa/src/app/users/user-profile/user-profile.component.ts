import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
})
export class UserProfileComponent implements OnInit {
  user: User = new User('peraperic', 'pera@gmail.com', 'Pera Peric', '/assets/default-user.png');
  inputFieldClass: string;

  constructor() {
    this.disableChangeFields();
  }

  ngOnInit() {}

  enableChangeFields() {
    this.inputFieldClass = 'field';
  }

  disableChangeFields() {
    this.inputFieldClass = 'disabled field';
  }

  onChangeName(event: Event) {
    const newName: string = (event.target as HTMLInputElement).value;
    this.user.name = newName;
  }
}
