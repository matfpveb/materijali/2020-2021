const express = require('express');
const users = require('../users/users');

const app = express();
const port = process.env.PORT || 3000;

app.set('view engine', 'ejs');
app.set('views', 'views/');

app.get('/api/users', (req, res) => {
  const allUsers = users.getAllUsers();
  res.render('users.ejs', { title: 'All users', users: allUsers });
});

app.listen(port, () => {
  console.log(`Aplikacija je aktivna na adresi http://localhost:${port}`);
});
