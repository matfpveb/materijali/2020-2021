import { Component, OnDestroy } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AuthService } from "../services/auth.service";
import { Subscription } from "rxjs";

@Component({
  selector: "app-register-form",
  templateUrl: "./register-form.component.html",
  styleUrls: ["./register-form.component.css"]
})
export class RegisterFormComponent implements OnDestroy {
  registerForm: FormGroup;
  registerSub: Subscription;

  constructor(private authService: AuthService) {
    this.registerForm = new FormGroup({
      username: new FormControl("", [Validators.required, Validators.pattern(/^[0-9a-zA-Z_-]{8,}$/)]),
      password: new FormControl("", [Validators.required, Validators.pattern(/^[0-9a-zA-Z_!@#$%^&*()+=`~-]{4,}$/)]),
      name: new FormControl("", Validators.required),
      email: new FormControl("", [Validators.required, Validators.email])
    });
  }

  public ngOnDestroy(): void {
    this.registerSub ? this.registerSub.unsubscribe() : null;
  }

  public register(): void {
    if (this.registerForm.invalid) {
      window.alert("The form is not valid!");
      return;
    }

    const formData = this.registerForm.value;
    this.registerSub = this.authService
      .registerUser(formData.username, formData.password, formData.name, formData.email)
      .subscribe();
  }
}
